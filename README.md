# SpaceInvaders.js
By Bernhard Ruckenstuhl and Simon Nowak

# About
This project was a game project for one of the courses (Mobile Development) in our University for our Bachelors Degree.
Even tho this is a "game", this has to be considered a tech demo, since the main goal of the course was a simple application with different Views and a slight logic.
Sadly we did not have enough time to make the TCP connection work for multiplayer gameplay over the same network.

# Features
* Load different Card Sets (in JSON format)
* Base Code for Multiplayer functionality (but not working)

# Bugs and missing Features
* Multiplayer Support not working in a full extend
* Multilanguage Support

# How to play
* Install the APK file in the "builds" folder on your android device
* Create a new game and start the game
* Select your best/funniest answer (white card) to the given (black) card >> After this the first round is finished. There is no feedback of who won the round, since multiplayer is not working.
* Select the best answer of the other "players" >> For now on 5 random cards are shown. This is in the game to simulate the selection of the winning cards.

# Screenshots
### Menu
![Menu Screenshot](https://bitbucket.org/Zumpel96/cards-and-humor/raw/df3d341f660102d4f46e22a9e02694c621d0fbc9/screenshots/Menu.jpg)

### Game Creation
![Game Creation Screenshot](https://bitbucket.org/Zumpel96/cards-and-humor/raw/df3d341f660102d4f46e22a9e02694c621d0fbc9/screenshots/Game%20Creation.jpg)

### Game Selection
![Game Selection Screenshot](https://bitbucket.org/Zumpel96/cards-and-humor/raw/df3d341f660102d4f46e22a9e02694c621d0fbc9/screenshots/Game%20Selection.jpg)

### Waiting for Players
![Waiting for Players Screenshot](https://bitbucket.org/Zumpel96/cards-and-humor/raw/df3d341f660102d4f46e22a9e02694c621d0fbc9/screenshots/Waiting%20for%20Players.jpg)

### Ingame
![Ingame Screenshot](https://bitbucket.org/Zumpel96/cards-and-humor/raw/df3d341f660102d4f46e22a9e02694c621d0fbc9/screenshots/Ingame.jpg)